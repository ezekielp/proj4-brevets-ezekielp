# Project 4: Brevet time calculator with Ajax

Author: Zeke Petersen     ezekielp@uoregon.edu

CIS 322 Fall 2019

Credits to Michal Young for the initial version of this code.

## ACP Calculator

This ACP calculator is intended to mimic the functionality of the RUSA calculator, but with an AJAX implementation. It will generate open and close times for the specified control points according to the algorithm described on the rusa.org website.

## Notes on the ACP controle times

The primary calculations use the following table:


| Control distance (km) | Minimum speed (km/hr) | Maximum speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
| 0-200                 | 15                    | 34                    |
| 200-400               | 15                    | 32                    |
| 400-600               | 15                    | 30                    |
| 600-1000              | 11.428                | 28                    |
| 1000-1300             | 13.333                | 26                    |


For each control point, open times are calculated using maximum speeds and close times are calculated using minimum speeds. The control distance is divided by the speed to get the time. For distances beyond 200 km, multimple speeds will be used in the calculation, each corresponding to the distance associated with it.

For example, a 350km control point close time will use 34km/hr for the first 200km and 32km/hr for the next 150km for a total time in hours of (200/34) + (150/32).

However, there are further rules and specifications that alter this algorithm slightly. They are as follows:

- The close time for the starting control point (0km) is always 1hr after the official start.

- In keeping with the French algorithm, for the first 60km, close times are based on a 20km minimum speed, plus 1hr (ex: A control point at 20km would be 20km/hr/20km + 1hr = 2hrs). After 60, it returns to the specified 15km minimum speed benchmark to calculate close times (without the extra hour). Calculation for the open times does not deviate from the table.

- Any control point may not be in excess of 120% the distance of the brevet distance (ex. a control point may not be >360km in a 300km brevet). If the control point is greater than 120% the brevet distance or less than 0, the application will default to the start time (for both open and close). Inputs may only be numbers.

- Miles are converted to the nearest kilometer before running the algorithm. Times are rounded to the nearest minute.

- The final close times for the 5 legal brevet distances are as follows (regardless of final control point location, as long as it is in the legal range):
	 200km -- 13h30
	 300km -- 20h00
	 400km -- 27h00
	 600km -- 40h00
	 1000km - 75h00
This means that a control point greater than or equal to the brevet distance and less than or equal to 120% the brevet distance will use the appropriate time as the close time.

- Daylight savings is not accounted for. That is, for days after "spring forward" and before "fall back", open and close times will have 1 hour added to the normal output (If the start time input is 1PM on May 9th, a control point at zero km will have an open time of 2PM and a close time of 3PM).
