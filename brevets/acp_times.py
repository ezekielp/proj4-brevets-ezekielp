"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


# Table values to be used for calculations (km: km/hr)
TABLE_MIN = {60: 20, 200: 15, 400: 15, 600: 15, 1000: 11.428, 1300: 13.333}
TABLE_MAX = {200: 34, 400: 32, 600: 30, 1000: 28, 1300: 26}

# Final times for the legal brevet distances (km: hr)
FINAL_TIMES = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    arw = arrow.get(brevet_start_time)
    arw.format('YYYY-MM-DD HH:mm')
    print(arw.isoformat())
    final = 0            # Number of hrs added to the start time (default to 0)
    c = control_dist_km  # Alias

    if (brevet_dist_km < c <= (1.2 * brevet_dist_km)):
        c = brevet_dist_km
    
    if (0 <= c <= (1.2 * brevet_dist_km)):
    # Case 1 - Control =< 200
    # Control / 34km/hr
        if (0 <= c <= 200):
            final = c / 34

    # Case 2 - Control 201-400
    # (Control - 200) / 32km/hr  +  (200/34)    
        elif (200 < c <= 400):
            final = ((c - 200) / 32) + (200 / 34)

    # Case 3 - Control 401-600
    # (Control - 400) / 30km/hr  +  (200/34) + (200/32)
        elif (400 < c <= 600):
            final = ((c - 400) / 30) + (200 / 34) + (200 / 32)

    # Case 3 - Control 601-1000
    # (Control - 600) / 28km/hr  +  (200/34) + (200/32) + (200/30)
        elif (600 < c <= 1000):
            final = ((c - 600) / 28) + (200 / 34) + (200 / 32) + (200 / 30)

    # Case 4 - Control 1001-1200
    # (Control - 1000) / 26km/hr + (200/34) + (200/32) + (200/30) + (400/28)
        elif (1000 < c <= 1200):
            final = ((c - 1000) / 26) + (200 / 34) + (200 / 32) + (200 / 30) + (400 / 28)

    else:
        final = 0
    
    hr = round(final)
    m = round((final - hr) * 60)
    x = arw.shift(hours=+hr).shift(minutes=+m)
    print(x.isoformat())
    return x.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    arw = arrow.get(brevet_start_time)
    arw.format('YYYY-MM-DD HH:mm')
    final = 0
    c = control_dist_km

    # If final control point, use appropriate value from table (brevet_dist_km
    # will be the key) to add to brevet_start_time
    if (brevet_dist_km < c <= (1.2 * brevet_dist_km)):
        for i in FINAL_TIMES:
            if brevet_dist_km == i:
                final = FINAL_TIMES[i]
                break

    # Case 1 - Control =< 60
    # Control / 20km/hr  +  1hr
    else:
        if (0 <= c <= (1.2 * brevet_dist_km)):
            if (0 <= c <= 60):
                final = (c / 20) + 1
    # Case 2 - Control 61-600
    # Control / 15km/hr
            elif (60 < c <= 600):
                final = c / 15

    # Case 3 - Control 601-1000
    # (Control - 600) / 11.428km/hr  +  40
            elif (600 < c <= 1000):
                final = ((c - 600) / 11.428) + 40

        else:
            final = 0

    hr = round(final)
    m = round((final - hr) * 60)
    x = arw.shift(hours=+hr).shift(minutes=+m)
    return x.isoformat()
