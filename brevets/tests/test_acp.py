"""
Nose tests for acp_times.py

Aiming to test all edge cases and expected usage.
"""

from acp_times import open_time, close_time

import nose    # Testing framework
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

time = arrow.get('2019-01-01 00:00', 'YYYY-MM-DD HH:mm')

def test_close_zero():
    new = time.shift(hours=+1)
    assert new.isoformat() == close_time(0, 200, time)
    assert new.isoformat() == close_time(0, 1000, time)
    
def test_open_zero():
    assert time.isoformat() == open_time(0, 400, time)
    assert time.isoformat() == open_time(0, 600, time)

def test_basic_close():
    new = time.shift(hours=+4)
    assert new.isoformat() == close_time(60, 200, time)
    new2 = time.shift(hours=+8)
    assert new2.isoformat() == close_time(120, 200, time)
    new3 = time.shift(hours=11).shift(minutes=+40)
    assert new3.isoformat() == close_time(175, 200, time)
    new4 = time.shift(hours=+13).shift(minutes=+30)
    assert new4.isoformat() == close_time(205, 200, time)
    
def test_basic_open():
    new = time.shift(hours=+1).shift(minutes=+46)
    assert new.isoformat() == open_time(60, 200, time)
    new2 = time.shift(hours=+3).shift(minutes=+32)
    assert new2.isoformat() == open_time(120, 200, time)
    new3 = time.shift(hours=+5).shift(minutes=+9)
    assert new3.isoformat() == open_time(175, 200, time)
    new4 = time.shift(hours=+5).shift(minutes=+53)
    assert new4.isoformat() == open_time(205, 200, time)

def test_specific():
    new = time.shift(hours=+29).shift(minutes=+9)
    assert new.isoformat() == open_time(890, 1000, time)
    new2 = time.shift(hours=+65).shift(minutes=+23)
    assert new2.isoformat() == close_time(890, 1000, time)
    new3 = time.shift(hours=+17).shift(minutes=+8)
    assert new3.isoformat() == open_time(550, 600, time)
    new4 = time.shift(hours=+10).shift(minutes=+34)
    assert new4.isoformat() == open_time(350, 600, time)
    new5 = time.shift(hours=+36).shift(minutes=+40)
    assert new5.isoformat() == close_time(550, 600, time)

def test_edge():
    assert time.isoformat() == open_time(2000, 200, time)
    assert time.isoformat() == close_time(-100, 1000, time)
    new2 = time.shift(hours=+40)
    assert new2.isoformat() == close_time(650, 600, time)
    new3 = time.shift(hours=+75)
    assert new3.isoformat() == close_time(1000, 1000, time)
